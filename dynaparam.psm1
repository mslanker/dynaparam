$MapFiles = @()
$Maps = @()
$MapFilesDirectory = "$PSScriptRoot/files/maps"
$LogsDirectory = "$PSScriptRoot/files/logs"

$Events = @("Created","Deleted","Renamed","Changed")

$FileSystemWatcher = New-Object System.IO.FileSystemWatcher
$FileSystemWatcher.Path = $MapFilesDirectory
$FileSystemWatcher.Filter = "*.json"
$FileSystemWatcher.NotifyFilter = [System.IO.NotifyFilters]'FileName,LastWrite'

$MessageData = @{LogsDirectory=$LogsDirectory}

foreach ($Event in $Events){
    if (Get-EventSubscriber -SourceIdentifier "File$Event" -ErrorAction SilentlyContinue) { Unregister-Event -SourceIdentifier "File$Event" }
    Register-ObjectEvent -InputObject $FileSystemWatcher -EventName $Event -SourceIdentifier "File$Event" -MessageData $MessageData -Action {
        $LogsDirectory = $Event.MessageData.LogsDirectory
        $LogFile = "$LogsDirectory/Events.log"
        $Name = $Event.SourceEventArgs.Name
        $ChangeType = $Event.SourceEventArgs.ChangeType
        $TimeStamp = $Event.TimeGenerated

        $Message = "The file '$Name' was $ChangeType at $TimeStamp"
        Add-Content -Value $Message -Path $LogFile -Force

        Build-MapCollection
    }
}

function Get-MapFiles {
    [CmdletBinding()]
    param(
        [string]$Path = "$PSScriptRoot/files/maps",
        [string]$Filter = "*.json"
    )

    Write-Verbose "Retrieving maps files from: $Path"
    $Script:MapFiles = Get-ChildItem -Path $Path -Filter $Filter
}

function Show-MapFiles {
    [CmdletBinding()]
    param()

    $Script:MapFiles
}

function Build-MapCollection {
    [CmdletBinding()]
    param(
        [string]$Path = "$PSScriptRoot/files/maps",
        [string]$Filter = "*.json"
    )

    Get-MapFiles -Path $Path -Filter $Filter

    $Maps = @()
    
    foreach ($Map in $MapFiles) {
        $Maps += Get-Content -Path $Map.FullName | ConvertFrom-Json
    }

    $Script:Maps = $Maps
    Build-IndexMap
}

function Update-MapCollection {
    [CmdletBinding()]
    param(
        [array]$MapCollection=$Script:Maps,
        [hashtable]$IndexMap=$Script:IndexMap,
        [array]$Data,
        [string]$IdKey="IdKey",
        [string]$PropKey="PropKey",
        [string]$ValueKey="ValueKey"
    )
    
    Write-Verbose "IdKey: $IdKey"
    Write-Verbose "PropKey: $PropKey"
    Write-Verbose "Value: $ValueKey"

    foreach ($Datum in $Data) {
        # Look up index by id
        $Index = $IndexMap.$($Datum.$IdKey)
        Write-Verbose "Updating $($Datum.$IdKey) - Index: $Index"
        $PropertyName = $Datum.$PropKey
        $UpdateValue = $Datum.$ValueKey
        Write-Verbose "Current $Propertyname value: $($MapCollection[$Index].$PropertyName)"
        Write-Verbose "New value $UpdateValue"
        $MapCollection[$Index].$PropertyName = $UpdateValue
    }
}

function Show-MapCollection {
    [CmdletBinding()]
    param()

    $Script:Maps
}

function Build-IndexMap {
    param()
    
    #Flush IndexMap
    $IndexMap = @{}

    if($Script:Maps) {
        $Index = 0
        foreach ($Map in $Script:Maps) {
            Write-Verbose "Adding: $($Map.CommandName) - Index: $Index"
            $null = $IndexMap.Add($Map.CommandName, $Index)
            $Index++
        }
    }

    $Script:IndexMap = $IndexMap
}

function Show-IndexMap {
    param()
    $Script:IndexMap
}

function Show-PSScriptRoot {
    "$PSScriptRoot"
}

function Generate-InvokeSomethingData {
    [CmdletBinding()]
    param(
        $Seed
    )

    $Data = @()

    foreach ($Key in $Seed.Keys) {
        $Dictionary = @{}
        $Dictionary.IdKey = $Key -replace "File", ""
        $Dictionary.PropKey = "FilePath"
        $Dictionary.ValueKey = $Seed.$Key

        $Data += $Dictionary
    }
    
    # return
    $Data
}

function Invoke-Something {
    [CmdletBinding()]
    param (
        
    )

    dynamicparam {
        # Propery name to use for parameter name
        $PropertyName = "CommandName"
        $Suffix = "File"
        # Create the collection of attributes
        $AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
        
        # Create and set the parameters' attributes
        $ParameterAttribute = New-Object System.Management.Automation.ParameterAttribute
        $ParameterAttribute.ParameterSetName = '_AllParameterSets'
        $ParameterAttribute.Mandatory = $false

        # Add the attributes to the attributes collection
        $AttributeCollection.Add($ParameterAttribute) 
        
        # Create the dictionary 
        $RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary

        # Generate and set the ValidateSet 
        # $ValidateAttribute = # Do Something Dynamic Here
        # $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($ValidateAttribute)    

        # Add the ValidateSet to the attributes collection
        #$AttributeCollection.Add($ValidateSetAttribute)
        
        # Iterate over collection building params
        foreach ($Map in $Maps)
        {
            if ($Map.$PropertyName) { $ParameterName = $Map.$PropertyName -replace " ", "" }
            else { Continue }

             # Create and return the dynamic parameter
            $RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter("$ParameterName$Suffix", [string], $AttributeCollection)
            $RuntimeParameter.Value = "Invalid"

            # Add the paramter to the dictionary
            $RuntimeParameterDictionary.Add("$ParameterName$Suffix", $RuntimeParameter)
        }

        # Return the dictionary
        $RuntimeParameterDictionary
    }
    begin {
    }
    
    process {
        $PSBoundParameters.Keys | % { Write-Verbose "Parameter: $($_) - Value: $($PSBoundParameters.$_)" }
        $Data = Generate-InvokeSomethingData -Seed $PSBoundParameters
        Update-MapCollection -Data $Data
    }
    
    end {
    }
}

Set-Alias -Name do-stuff -Value Invoke-Something
Set-Alias -Name ds -Value Invoke-Something

# Build Map Collection on  module load
Build-MapCollection